# mini-project7-yg229

# Qdrant Binary Vector Search Project

## Summary

This project provides an example implementation of binary vector search using the Qdrant search engine. It demonstrates how to convert binary data into vectors, ingest them into a Qdrant collection, and perform vector searches using the Euclidean distance metric.

## Features

- Conversion of binary strings to floating-point vectors for search engine compatibility.
- Creation of a new Qdrant collection tailored for 6-dimensional vectors.
- Ingestion of binary vector data into the Qdrant collection with associated item identifiers.
- Performing vector search queries and retrieving the most similar items based on the Euclidean distance metric.

## Implementation Steps

### Environment Setup

Before running the project, ensure you have the following prerequisites installed:

- Rust programming language environment
- Tokio async runtime library
- Qdrant client for Rust

### Qdrant Server

Make sure your Qdrant server is up and running and accessible at the provided endpoint (`http://localhost:6334` by default).

```docker run -p 6333:6333 -p 6334:6334 -e QDRANT__SERVICE__GRPC_PORT="6334" qdrant/qdrant```
![](1.png)

### Project Structure

The project is structured into the following main parts:

- `binary_string_to_vector`: A utility function to convert binary strings to vector representations.
- `ingest_data`: An asynchronous function that takes a mutable reference to a Qdrant client and a collection name to ingest data into Qdrant.
- `search_query`: An asynchronous function that takes a mutable reference to a Qdrant client and a collection name to perform search queries in Qdrant.
- `main`: The entry point of the application that initializes the Qdrant client, creates the collection, ingests data, and performs a search query.

### Running the Project

To run the project, use the following commands in your terminal:

1. Compile the project with `cargo build`.
2. Run the project with `cargo run`.

### Data Ingestion

Upon execution, the `ingest_data` function will ingest the following binary data into the Qdrant collection: `"110111"`, `"111011"`, `"100011"`, `"111111"`.

### Search Query

The `search_query` function performs a search using `"111011"` as the query vector and retrieves the most similar items based on the Euclidean distance metric.

## Contact

For support or contributions, please contact yg229@duke.edu

## Results
![](2.png)
## For next test, please remember to delete the collection before continue
![](3.png)