use tokio;
use serde_json::json;
use qdrant_client::prelude::*;
use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{CreateCollection, PointStruct, SearchPoints};
use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig};
use anyhow::Result; // Keep anyhow if you plan to use it for error handling

// Function to convert a binary string to a vector of f32
fn binary_string_to_vector(binary_str: &str) -> Vec<f32> {
    binary_str.chars().map(|c| c.to_digit(10).unwrap() as f32).collect()
}

// Function to ingest binary data into Qdrant
async fn ingest_data(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    let binary_data = vec!["110111", "111011", "100011", "111111"];

    for (i, binary_str) in binary_data.iter().enumerate() {
        let vector = binary_string_to_vector(binary_str);
        let points = vec![
            PointStruct::new(
                i as u64 + 1, // ItemID, ensuring it starts from 1
                vector, // Vector representation of the binary data
                json!({"ItemID": i + 1}) // Payload with ItemID
                .try_into()
                .unwrap(),
            ),
        ];
        client.upsert_points_blocking(collection_name, None, points, None).await?;

        println!("Data ingest successful for ItemID {}...", i + 1);
    }
    Ok(())
}

// Function to perform a search query in Qdrant
async fn search_query(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    let query_vector = binary_string_to_vector("111011");

    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: query_vector,
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

        for (index, point) in search_result.result.iter().enumerate() {
            println!(
                "Point {} ItemID: {:?}  Euclidean distance {}",
                index + 1,
                point.id,
                point.score
            );
        }
        
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    // The Rust client uses Qdrant's GRPC interface to initialize
    let mut client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Create a collection
    let collection_name = "Hamming_code_collection";

    client.
        create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 6,
                    distance: Distance::Euclid.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    // Ingest data into Qdrant
    ingest_data(&mut client, collection_name).await?;

    // Perform a search query
    search_query(&mut client, collection_name).await?;

    Ok(())
}
